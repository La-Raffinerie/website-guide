+++
outputs = ["Reveal"]
title = "Guide de la coopérative"

+++
# Le Guide de la Coopérative

« Nout reset ! » _(notre recette)_

***

###### Tu es en recherche de sens ?

Tu veux changer le monde, réinventer le travail dans un collectif attentif à l’humain et l’environnement ? Et en plus, tu veux "mèt kok dann marmite" à la fin du mois ? Ce document te dira tout sur comment rejoindre l’équipe de la coopérative de la Raffinerie.

***

# LE PLAN

#### // « Il était une fois…» La Raffinerie !

#### // La coopérative : Kosa i lé ?

⇨ **Au menu** : Le projet SCIC de La Raffinerie<br>
⇨ **Ingrédients de base** : raison d'être et valeurs<br>
⇨ **Déroulé de la recette** : travailler autrement<br>
⇨ **Les ustensiles** : les outils de la gouvernance

#### // Comment on gère collectivement la "cuisine"

#### // Et ma part du gâteau dans tout ça ?

***

{{% section %}}

# « Il était une fois… » La Raffinerie !

le projet de La Raffinerie est dans les cartons depuis 2015. En 2019 Le conseil Départemental de La Réunion nous fait confiance pour concrétiser la mise en place d'[**un Tiers Lieu**](https://documentation.laraffinerie.re/index.php/C%27est_quoi_un_Tiers_Lieu%3F) multidisciplinaire sur le site de l'ancienne usine sucrière de Savanna à Saint-Paul.

## **↓**

***

###### Mais c'est quoi un Tiers Lieux ?

Un tiers-lieu est l’incarnation, dans un espace d’activités marchandes ou non marchandes, d’un contrat social qui se décompose à travers trois dimensions :

⇨ UN PARCOURS D’ÉMANCIPATION INDIVIDUELLE    <br>
⇨ UNE DYNAMIQUE COLLECTIVE                   <br>
⇨ UNE DÉMARCHE MOTIVÉE PAR L’INTÉRÊT GÉNÉRAL.

## **↓**

***

[<img src=uploads/c-quoi-un-tiers-lieux.jpg height=500></img>](uploads/c-quoi-un-tiers-lieux.jpg "charte")

## **↓**

***

###### C'est quoi exactement les missions de La Raffinerie ?

La friche éco-culturelle "La Raffinerie" ambitionne d’offrir les informations, les formations, les espaces, les activités et les outils

pour permettre aux utilisateurs.ices d'être acteurs de l'amélioration de leur quotidien tout en répondant au mieux aux urgences environnementales et socio-économiques actuelles.

## **↓**

***

[<img src=uploads/charte.jpg height=500></img>](uploads/charte.jpg "charte")

## **↓**

***

###### et si il fallait résumer tout ça en quelques mots?

> Économie circulaire, économie sociale et solidaire, développement durable, transition écologique, biodiversité et culture sont au cœur des activités organisées autour de différents groupes qui fonctionnent en interaction les uns avec les autres.

## **↓**

***

# ça a l'air tentant! ...

# concrètement c'est quoi les activités?

## **↓**

***

* **Le Groupe micro-recyclerie** avec des activités de vélo, D3E, métal / bois, fablab, édition, ateliers marmailles & couture;
* **Le Groupe jardin** avec le verger / potager, la micro-forêt, le jardin d'agrément, le rucher & la serre aquaponique;
* **Le Groupe Alimentation** avec le café / restaurant / le laboratoire de transformation alimentaire / l'épicerie en vrac & l'AMAP
* **Le Groupe Sport** avec le mur d'escalade, le terrain de pétanque, le bowl de skate, l'espace sport santé et le workout
* **Le groupe Culture** avec les festivals, le café culturel et le culture Lab
* **Le groupe Économique** avec le café/resto, la micro brasserie, la location d'espaces & la formation.

## **↓**

***

[<img src=uploads/plan.jpg height=500></img>](uploads/plan.jpg "charte")

## **→**

{{% /section %}}

***

{{% section %}}

# La coopérative...

# Kosa i lé ?

Recette pour 1 Gato "Kane à SCIC"!

***

### Au Menu

### Le projet SCIC de La Raffinerie

###### c'est quoi les missions de cette SCIC?

> La SCIC est La coopérative qui aura pour objectif de gérer le lieu, de fédérer les différents.es chargés.es de mission, de porter les activités lucratives et de supporter les différentes charges du projet.

## **↓**

***

# Une SCIC ?

# Kezako ?

## **↓**

***

C’est une Société Coopérative d’Intérêt Collectif. Mais, brièvement, qu’est-ce qu’une SCIC ? Une SCIC est une entreprise (SARL, SAS, SA …). Elle peut concerner tous les secteurs d'activités, et associe des personnes physiques ou morales, de droit privé ou de droit public dès lors que l'intérêt collectif se justifie autour [**d’un projet commun**](https://documentation.laraffinerie.re/index.php/Charte_des_valeurs) alliant efficacité économique, développement local et utilité sociale et environnementale.

<a href="https://documentation.laraffinerie.re/index.php/C%27est_quoi_une_SCIC%3F" target="_blank">En savoir plus le fonctionnement d'une SCIC </a>

## **↓**

***

# Pourquoi créer une SCIC au sein de La Raffinerie ?

## **↓**

***

###### La SCIC de La Raffinerie a pour but de

* fédérer les chargés.es de projets qui feront vivre le site
* organiser et structurer au niveau juridique, administratif et commercial les activités de La Raffinerie (bar, restaurant, micro-brasserie, labo de transformation, espace co-working, formations...),
* de supporter les différentes charges du projet (entretien des espaces, des locaux...).

## **↓**

***

###### La SCIC est également la seule forme juridique d'entreprise qui permette :

* D'associer toutes les parties prenantes de son territoire
* De fédérer différents acteurs.rices qui œuvrent pour le développement de l'entreprise, dans une perspective d'intérêt collectif,
* D’avoir une gouvernance permettant d'associer les travailleurs, citoyens, voisins et partenaires,
* De rémunérer les travailleurs.euses indépendantses (= « Les Raffineurs.euses ») qui réalisent des missions tout en pouvant participer à la gouvernance,
* De ne pas avoir de limite dans la réalisation de chiffre d'affaire…

## **→**

{{% /section %}}

***

{{% section %}}

### Ingrédients de base :

> ⇒ Pour réussir cette recette, il faut de bons ingrédients ! Ces ingrédients sont le fruit de plusieurs semaines de réflexion de la part des premiers membres.

## **↓**

***

## Ingrédient n°1

###### ⇒ Notre raison d’être

c'est le rôle et la vision à long terme que nous voulons avoir dans la société

> « Ré-inventer le travail dans un collectif attentif à l’humain et à l’environnement au service du projet de la Raffinerie »

## **↓**

***

## Ingrédient n°2

###### ⇒ Les Valeurs

Nos valeurs fondamentales reposent sur trois piliers :<br>

* la coopération   <br>
* l'expérimentation<br>
* l'engagement sociétal et environnemental

## **↓**

***

###### la coopération

> Bienveillance, équité, transparence, respect de la différence, compréhension et indulgence, épanouissement au travail, confiance et entraide, transmission possible de ses compétences

## **↓**

***

###### l'expérimentation

> essai de nouvelles manières de faire, créativité, innovation, droit à l’erreur

## **↓**

***

###### l'engagement sociétal et environnemental

> Comportement respectueux du vivant, conscience écologique, maintien et restauration de la diversité, limitation de notre impact sur l’environnement, ancrage dans le territoire (du micro local à l’échelle régionale), résilience (peut se rétablir et/ou s’adapter après une perturbation de l’équilibre interne ou externe)

## **→**

{{% /section %}}

***

{{% section %}}

### Le déroulé de la recette :

⇒ Ensuite, il faut savoir bien mélanger ces ingrédients grâce à notre conception du travailler autrement!

Nous avons choisi de nous organiser suivant le modèle du travailleur.euse indépendant.e, plutôt que celui du salarié.e, mais pourquoi??

## **↓**

***

###### pour plusieurs raisons :

* Au démarrage du projet, nous n’aurons pas l’activité suffisante pour assurer des temps plein dans tous les pôles et peut donc permettre de démarrer avec un maximum de coopérateurs.rices!
* Cela peut permettre également d'être polyvalent.e et varier les types et sujets de missions, afin de rester motivé.e et ne pas tomber dans la routine d'une tâche répétitive !
* Nous faisons beaucoup plus confiance à l'humain et à la motivation qu'au diplôme, l'expérience professionnelle ou ancienneté

## **↓**

***

* Ce statut permet au travailleur.euse de ne pas avoir de subordination et être libre dans ses horaires, dans les missions qu'il/elle choisit et dans le déroulement de celles-ci, de mieux équilibrer sa vie personnelle et professionnelle... de travailler différemment et plus librement !
* A La Raffinerie, il y a même la possibilité de monter ses propres projets et créer ses propres missions!

## **↓**

***

# très bien, mais il n'y a pas que des avantages!

## **↓**

***

###### Effectivement

il y a aussi des freins à devenir travailleur.euse indépendant.e comme une certaine incertitude dans les premières années d'exercice…

C'est pourquoi au sein de notre SCIC, nous souhaitons réfléchir à des outils et dispositifs afin d'accompagner au mieux les chargés.es de missions comme :

* le calcul du bonheur intérieur brut
* la validation des "compétences"
* la création d'un fond de solidarité
* l'accès à une mutuelle ou à des assurances, ...

Et cela se fera à notre rythme, avec votre aide et votre investissement...

## **→**

{{% /section %}}

***

{{% section %}}

### Les ustensiles

> ⇒ pas de bonnes recettes sans bons outils!

Ces outils concernent la gouvernance avec les principes de la [**stigmergie** ](https://documentation.laraffinerie.re/index.php/La_stigmergie)& la prise de décision par consentement et l'organisation avec la [**co-rémunération**]() et les [**open-badges**]().

Ce sont donc les outils avec lesquels nous souhaitons rélaliser notre recette !

## **↓**

***

###### la Stigmergie

> Entre le modèle concurrentiel et le modèle consensuel, la stigmergie, une nouvelle méthode de gouvernance inspirée du mode d’organisation des insectes sociaux, pourrait offrir un modèle alternatif plus adapté à la collaboration dans des grands groupes.
>
> <a href="https://documentation.laraffinerie.re/index.php/La_stigmergie" target="_blank">En savoir +</a>

## **↓**

***

###### la prise de décision par "consentement"

> Le consentement implique qu’une décision ne peut être prise que lorsqu’il n’y a plus d’objection raisonnable à celle-ci. Tant qu’il y a des objections, l’ensemble du groupe est mobilisé pour bonifier la proposition. Nous tendons vers cela, et aujourd'hui, nous fonctionnons avec la sollicitation d'avis.
>
> <a href="https://documentation.laraffinerie.re/index.php/La_gestion_par_consentement" target="_blank">En savoir +</a>

## **↓**

***

###### la Co-Rémunération

> Le principe de la co-rémunération repose sur le principe d'un budget contributif qui va permettre de rétribuer les membres participants selon leur implication dans le projet. Chaque membre pourra ainsi prélever sa rémunération selon ce qu’il estime juste, et ce en l’affichant en toute transparence au reste du collectif.
>
> <a href="https://documentation.laraffinerie.re/index.php/La_co-r%C3%A9mun%C3%A9ration" target="_blank">En savoir +</a>

## **↓**

***

###### Les Open-badges

> Les Badges sont utilisés pour confirmer l'acquisition d'aptitudes, de connaissances ou de compétences qui ne sont pas reconnues par un diplôme ou une certification formelle.
>
> <a href="https://documentation.laraffinerie.re/index.php/Les_Open_Badge_de_La_Raffinerie" target="_blank">En savoir +</a>

## **→**

{{% /section %}}

***

# Comment ça se gère collectivement ?

La Raffinerie, appartient à tous les sociétaires,

* ils profitent de ses services mais sont également solidaires dans la gestion globale du site.
* Ils sont bienveillant.e.s et si il y a des problèmes dans d'autres pôles ils sont toujours prêts à donner un coup de main.
* Ils sont aussi toujours en alerte pour que le site soit le plus accueillant et agréable possible pour ses activités ou pour les visiteurs.

***

# Et ma part de gâteau dans tout ça ?

Comment avoir ta part du gato Kane a SCIC :

* en obtenant mon badge de Raffineur.euse!
* en utilisant la boite à outils de La Raffinerie pour faire des missions ou en créer (pour moi et les autres Raffineurs.euses)
* Si mes activités sont moins lucratives, en profitant du partage des ressources issues des activités les plus lucratives

***

# Est ce que cette dynamique est faite pour toi?

> Tu veux passer un bon moment et savoir si tu t’embarques avec nous ? Fais le Raffi-QUIZZ
>
> <a href=https://mooc.laraffinerie.re target="_blank">lien vers le quizz!</a>

***

# EPILOGUE…

> Tu as réussi à lire tout le document ? à répondre au questionnaire, à parcourir la <a href=https://documentation.laraffinerie.re/index.php/Accueil target="_blank">documenterie</a> ?

###### BRAVO !

> Tu viens de gagner ton 1er badge ! C’est le début d’une longue histoire, qui feront de toi un.e Raffineur.euse curieux.se, aidant.e, impliqué.e, autonome et enfin expert.e !
>
> <a href=https://mooc.laraffinerie.re target="_blank">clique pour obtenir ton badge</a>

{{% section %}}